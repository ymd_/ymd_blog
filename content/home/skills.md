+++
# A Skills section created with the Featurette widget.
widget = "featurette"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 80  # Order that this section will appear.

title = "Skills"
subtitle = ""

# Showcase personal skills or business features.
# 
# Add/remove as many `[[feature]]` blocks below as you like.
# 
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons

[[feature]]
  icon = "chart-line"
  icon_pack = "fas"
  name = "Data Science"
  description = "Statistics/Machine Learning"

[[feature]]
  icon = "terminal"
  icon_pack = "fas"
  name = "Programming"
  description = "C++/Python (Cython)/shell script/JavaScript/Emacs Lisp/C#"

[[feature]]
  icon = "cubes"
  icon_pack = "fas"
  name = "Cloud Native Computing"
  description = "Docker/Kubernetes"

+++
