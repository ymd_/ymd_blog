---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "b4tf"
summary: "Bayes Neural Network for TensorFlow"
authors: ["ymd"]
tags: ["Python","b4tf","Bayes","machine_learning"]
categories: []
date: 2020-09-20T09:44:49+09:00

# Optional external URL for project (replaces project detail page).
external_link: ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: "Smart"
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
links:
- name: GitLab
  url: https://gitlab.com/ymd_h/b4tf
  icon_pack: fab
  icon: gitlab
- name: GitHub
  url: https://githab.com/ymd-h/b4tf
  icon_pack: fab
  icon: github
- name: PyPI
  url: https://pypi.org/project/b4tf/
  icon_pack: fas
  icon: cubes

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---

b4tf is a Python module providing Bayes neural network based on
[TensorFlow 2](https://www.tensorflow.org/) and [TensorFlow
Probability](https://www.tensorflow.org/probability).
