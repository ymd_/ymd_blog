(function(){
    const path = location.pathname
    const gl_base = "https://gitlab.com/api/v4/projects/"
    const gh_base = "https://api.github.com/repos/"

    if(path.match("/project/cpprb/")){
	var gl = $.get(gl_base + "ymd_h%2Fcpprb");
	var gh = $.get(gh_base + "ymd-h/cpprb");
    }else if(path.match("/project/tf2rl/")){
	var gl = false
	var gh = $.get(gh_bse + "keiohta/tf2rl");
    }else if(path.match("/project/ymd_cd/")){
	var gl = false
	var gh = $.get(gh_base + "ymd-h/ymd_cd");
    }else if(path.match("/project/gym-notebook-wrapper/")){
	var gl = $.get(gl_base + "ymd_h%2Fgym-notebook-wrapper")
	var gh = $.get(gh_base + "ymd-h/gym-notebook-wrapper");
    }else if(path.match("/project/b4tf/")){
	var gl = $.get(gl_base + "ymd_h%2Fb4tf")
	var gh = $.get(gh_base + "ymd-h/b4tf")
    }

    $(document).ready(function(){
	if(gl){
	    gl.then((data)=>{
		$("a.btn > i.fa-gitlab").parent()
		    .append("(<i class='fas fa-star fa-fw'></i>"
			    + data["star_count"]
			    + "<i class='fas fa-code-branch fa-fw'></i>"
			    + data["forks_count"]
			    + ")");
	    });
	}

	if(gh){
	    gh.then((data)=>{
		$("a.btn > i.fa-github").parent()
		    .append("(<i class='fas fa-star fa-fw'></i>"
			    + data["stargazers_count"]
			    + "<i class='fas fa-code-branch fa-fw'></i>"
			    + data["forks_count"]
			    + ")");
	    });
	}
    });
})();
